def Order_Energy(population):
    energy_list = []
    for member in population:
        energy_list.append(member.energy)
    reorder_pop = [x for (y, x) in sorted(zip(energy_list, population))]
    return reorder_pop
    