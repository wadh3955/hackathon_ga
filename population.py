import Cluster_type
import Mating
import fitness_test
import random
import order_energy
import numpy as np

class Population_Class:

    def __init__(self, Nclusters, Natoms, radius1):
        self.population=[]
        for i in xrange(Nclusters):
            self.population.append(Cluster_type.Cluster(Natoms,radius1))
        self.Nclusters = Nclusters

    def select_parents(self):
        Parent1_index = random.randint(0,self.Nclusters-1)
        Parent2_index = random.randint(0,self.Nclusters-1)
        Parent1 = self.population[Parent1_index]
        Parent2 = self.population[Parent2_index]
        return Parent1, Parent2

    def Lowest_Energy(self):
        self.population = order_energy.Order_Energy(self.population)
        return self.population[0]

    def Highest_Energy(self):
        self.population = order_energy.Order_Energy(self.population)
        return self.population[self.Nclusters-1]

    #def Order_Energy(self):
    #    for member in self.population:
    #        energy_list=[]
    #        energy_list.append(member.energy)
    #    self.population=[x for(y,x) in sorted(zip(energy_list,self.population))]
    #        energy_list.sort()

    def update(self):
        P1 , P2 = self.select_parents()
        Child = Mating.Mate(P1, P2)
        Child.cluster_mutation()
        # Child.optimize()
        self.population = fitness_test.selection_criteria(Child, self.population)




if __name__ == "__main__" :
    popu = Population_Class(10,2,1.0)
    for i in range(10):
        print popu.population[i].energy


    popu.update()
    print "---------------------"
    for i in range(10):
        print popu.population[i].energy
    print "---------------------"
    lowest_en = popu.Lowest_Energy()
    print lowest_en.energy


