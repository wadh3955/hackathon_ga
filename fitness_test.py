# Fitness test
import numpy as np
from similarity import similarity_test
from Cluster_energy import energy
import Cluster_type

# Function to find the average displacement from the centre of mass of the current population
def average_displacement(population):
    # Find number of clusters and add all displacements to list
    N = len(population)
    dlist = []
    for cluster in population:
        cluster.displacement_update()
        d = cluster.displacement
        dlist.append(d)
    # Calculate dmin from the list
    dlist = np.array(dlist)
    davg = np.sum(dlist)/(N*1.0)
    dmin = davg/4.0
    return dmin

# Function to find the worst member (highest energy) of the current population
def find_worst(population):
    # Loop through population list and compare the energies of each member
    i=0
    w=0
    energy_worst = population[i].energy
    for cluster in population:
        if cluster.energy > energy_worst:
            energy_worst=population[i].energy
            w = i
        i += 1
    # Assign worst member value and return it
    worst = population[w]
    return worst

# Function to determine whether a parent is replaced by a child in the population
def selection_criteria(child, population):
    # Call functions to find dmin, d3, worst population member, and find cluster energies
    worst = find_worst(population)
    dmin = average_displacement(population)
    energy_child = energy(child)
    energy_worst = energy(worst)
    d3 = similarity_test(child, worst)
    # If conditions satisfied then add child to the population
    if d3 <= dmin and energy_child < energy_worst:
        population.remove(worst)
        population.append(child)
    return population



