import pyglet
from pyglet.gl import *
from math import *
import Cluster_type
import population
import numpy as np


def makeCircle(numPoints, radius, xcenter, ycenter):
    '''draws cicrles by proding as a set of points in a cicrle and joing ing as a line'''
    vertices = []
    for i in range(numPoints):
        angle = radians(float(i)/numPoints * 360.0)
        x = radius*cos(angle) + xcenter
        y = radius*sin(angle) + ycenter
        vertices += [x,y]
    circle = pyglet.graphics.vertex_list(numPoints, ('v2f', vertices))
    return circle

class drawWindow(pyglet.window.Window):
    def __init__(self):
        super(drawWindow, self).__init__()
        self.popu = population.Population_Class(100,3,1.0)
        ##import highest energy and lowest energy cluster
        self.low_cluster = self.popu.Lowest_Energy()
        self.high_cluster = self.popu.Highest_Energy()
        self.generation =0

        self.drawList_low = [0] * self.low_cluster.Natoms
        self.drawList_high = [0] * self.high_cluster.Natoms

        self.scale = 0.5* self.height / self.low_cluster.box_len

        ##define lowest energy centers
        low_average =[np.average(self.low_cluster.config[:,0]),np.average(self.low_cluster.config[:,1])] # find avergae value of x in cluster
        self.low_centers= np.zeros((self.low_cluster.Natoms, 2)).tolist()                       # list of the centres of the atoms
        for i in xrange(self.low_cluster.Natoms):
            self.low_centers[i]=[self.scale * (self.low_cluster.config[i, 0] - low_average[0]), self.scale * (self.low_cluster.config[i, 1] - low_average[1])] # adds data to centers and scales the values for the windows
        self.label_low_eng = pyglet.text.Label(text='Lowest Energy: ' + str("{0:.4f}".format(self.low_cluster.energy)),
                                               font_size=16)

        ##define highest energy centers
        high_average = [np.average(self.high_cluster.config[:, 0]), np.average(self.high_cluster.config[:, 1])]  # find avergae value of x in cluster
        self.high_centers = np.zeros((self.high_cluster.Natoms, 2)).tolist()  # list of the centres of the atoms
        for i in xrange(self.low_cluster.Natoms):
            self.high_centers[i] = [self.scale * (self.high_cluster.config[i, 0] - high_average[0]), self.scale * (self.high_cluster.config[i, 1] - high_average[1])]  # adds data to centers and scales the values for the windows
        self.label_high_eng = pyglet.text.Label(text='Highest Energy: ' + str("{0:.4f}".format(self.high_cluster.energy)),
                                               font_size=16, x=self.width*0.5)

        self.label_generation = pyglet.text.Label(text ='Generation ' +str(self.generation),
                                               font_size=16, x=self.width*0.5, y=self.height )

    def on_draw(self):
        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)  # clear the graphics buffer

        #### Make cicrle of each atom in the lowest energy structure
        for i in xrange(self.low_cluster.Natoms):
            if (self.low_cluster.config[i, 2] < 0.5):
                self.drawList_low[i] = makeCircle(200, self.scale / 2, self.low_centers[i][0] + 0.25*self.width, self.low_centers[i][1] + 0.5*self.height)  # populate the drawList with A atoms
            else:
                self.drawList_low[i] = makeCircle(200, self.scale * self.low_cluster.rad1 / 2, self.low_centers[i][0] + 0.25*self.width, self.low_centers[i][1] + 0.5*self.height) # populates the dawList with B atoms


        #### draw each of the cicrles for lowest energy structure
        for i in xrange(self.low_cluster.Natoms):
            if (self.low_cluster.config[i, 2] < 0.5):
                glColor3f(1,1,0)                        # specify colors & draw, draws A atoms
                self.drawList_low[i].draw(GL_LINE_LOOP)
            else:
                glColor3f(1, 0, 1)                      # specify colors & draw, draws B atoms
                self.drawList_low[i].draw(GL_LINE_LOOP)
        self.label_low_eng.draw()

        #### Make cicrle of each atom in the highest energy structure
        for i in xrange(self.high_cluster.Natoms):
            if (self.high_cluster.config[i, 2] < 0.5):
                self.drawList_high[i] = makeCircle(200, self.scale / 2, self.high_centers[i][0] + 0.75*self.width,
                                                  self.high_centers[i][
                                                      1] + 0.5*self.height)  # populate the drawList with A atoms
            else:
                self.drawList_high[i] = makeCircle(200, self.scale * self.high_cluster.rad1 / 2,
                                                  self.high_centers[i][0] + 0.75*self.width, self.high_centers[i][
                                                      1] + 0.5*self.height)  # populates the dawList with B atoms

        #### draw each of the cicrles for highest energy structure
        for i in xrange(self.high_cluster.Natoms):
            if (self.high_cluster.config[i, 2] < 0.5):
                glColor3f(1, 1, 0)  # specify colors & draw, draws A atoms
                self.drawList_high[i].draw(GL_LINE_LOOP)
            else:
                glColor3f(1, 0, 1)  # specify colors & draw, draws B atoms
                self.drawList_high[i].draw(GL_LINE_LOOP)
        self.label_high_eng.draw()

        self.label_generation.draw()

    def update(self,dt): #generate new cluster and updates centers list
         self.generation += 1

         ### update population
         self.popu.update()
         self.low_cluster = self.popu.Lowest_Energy()
         self.high_cluster = self.popu.Highest_Energy()

         ### update lowest energy structures
         low_average = [np.average(self.low_cluster.config[:, 0]), np.average(self.low_cluster.config[:, 1])]  # find avergae value of x in cluster
         for i in xrange(self.low_cluster.Natoms):
             self.low_centers[i] = [self.scale * (self.low_cluster.config[i, 0] - low_average[0]), self.scale * (self.low_cluster.config[i, 1] - low_average[1])]  # adds data to centers and scales the values for the windows
         self.label_low_eng = pyglet.text.Label(text='Lowest Energy: ' + str("{0:.4f}".format(self.low_cluster.energy)),
                                                font_size=16, x=0.25*self.width, anchor_x="center")

         ### update highest energy structures
         high_average = [np.average(self.high_cluster.config[:, 0]), np.average(self.high_cluster.config[:, 1])]  # find avergae value of x in cluster
         for i in xrange(self.low_cluster.Natoms):
             self.high_centers[i] = [self.scale * (self.high_cluster.config[i, 0] - high_average[0]), self.scale * (self.high_cluster.config[i, 1] - high_average[1])]  # adds data to centers and scales the values for the windows
         self.label_high_eng = pyglet.text.Label(text='Highest Energy: ' + str("{0:.4f}".format(self.high_cluster.energy)),
                                                 font_size=16, x=self.width*0.75, anchor_x="center")



         self.label_generation = pyglet.text.Label(text='Generation ' + str(self.generation),
                                                   font_size=16, x=self.width*0.5 , y=self.height-20, anchor_x='center')



if __name__ == '__main__':
    window = drawWindow()  # initialize a window class
    pyglet.clock.schedule(window.update)  # tell pyglet how often it should execute on_draw() & update()
    pyglet.app.run()  # run pyglet