import Cluster_type
import random
import numpy as np

def gaussian(rad1,Natoms):
    x=random.random()
    y=random.random()
    sigmaBB=rad1
    gauss = np.sqrt(-2*np.log(x))*np.cos(2*(np.pi)*y)*0.05*sigmaBB*2**(1/6)*np.sqrt(Natoms)
    return gauss

def mutation(config, rad1, Natoms):
    prob=0.1
    for atom in config:
        if random.random()<prob:
            x_distortion=gaussian(rad1, Natoms)
            y_distortion=gaussian(rad1, Natoms)
            atom[0]+=x_distortion
            atom[1]+=y_distortion
        if random.random()<prob:
            if atom[2] < 0.5 :
                atom[2]= 1
            if atom[2] > 0.5 :
                atom[2]=0
    return config


if __name__ == "__main__" :
    cluster = Cluster_type.Cluster(2, 1)
    print 'cluster has configuration', cluster.config
    #mutation(cluster)
    cluster.config = mutation(cluster.config)
    print 'mutated cluster has configuration', cluster.config
