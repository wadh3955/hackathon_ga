import pyglet
from pyglet.gl import *
from math import *
import Cluster_type

'''make cluster'''
cluster = Cluster_type.Cluster(6, 1)

def makeCircle(numPoints, radius, xcenter, ycenter):
    vertices = []
    for i in range(numPoints):
        angle = radians(float(i)/numPoints * 360.0)
        x = radius*cos(angle) + xcenter
        y = radius*sin(angle) + ycenter
        vertices += [x,y]
    circle = pyglet.graphics.vertex_list(numPoints, ('v2f', vertices))
    return circle

class drawWindow(pyglet.window.Window):
    def __init__(self):
        super(drawWindow, self).__init__()
        self.ncircles = cluster.Natoms
        self.drawList = [0]*self.ncircles
        self.centers=[]
        for i in xrange(cluster.Natoms):
            self.centers.append([self.width*cluster.config[i,0]/(2.0*cluster.Natoms),self.width*cluster.config[i,1]/(2.0*cluster.Natoms)])


        self.center1 = [self.width/2,self.height/2]
        self.center2 = [self.width/2,self.height/2]

    def on_draw(self):
        self.drawList[0] = makeCircle(100, 20, self.center1[0], self.center1[1])  # populate the drawList
        self.drawList[1] = makeCircle(100, 50, self.center2[0], self.center2[1])

        glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)  # clear the graphics buffer
        glColor3f(1,1,0)                        # specify colors & draw
        self.drawList[0].draw(GL_LINE_LOOP)
        glColor3f(0.5,0,1)                      # specify colors & draw
        self.drawList[1].draw(GL_LINE_LOOP)

    def update(self,dt):
        #print(dt) # time elapsed since last time a draw was called
        print "Updating the centers of the circles"
        self.center1 = [window.width/2, window.height/2]
        self.center2 = [window.width/2, window.height/2]
        print "Finished update"

if __name__ == '__main__':
    window = drawWindow()                                 # initialize a window class
    pyglet.clock.schedule_interval(window.update, 1/2.0)  # tell pyglet how often it should execute on_draw() & update()
    pyglet.app.run()                                      # run pyglet