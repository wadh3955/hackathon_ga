import numpy as np
import sys
import random
from sobol import *

class Cluster:
    def __init__(self, Natoms,radius1):
        self.rad1 = radius1
        self.Natoms = Natoms
        self.box_len = np.sqrt(self.Natoms*np.pi*5*(self.rad1*1.123/2)**2)
        config = []
        seed = random.randint(100,1000)    # produce random seed for sobol sequence
        for i in range(Natoms):
            cooridnates, seed = i4_sobol(2, seed)  # produce sobol sequence, these are pseudo random number that are more uniformly spaced
            if random.random() > 0.5:
                config.append([self.box_len*cooridnates[0],self.box_len*cooridnates[1],1])
            else:
                config.append([self.box_len*cooridnates[0],self.box_len*cooridnates[1], 0])
        self.config=np.array(config)
        self.energy =  0
    def cluster_check(self):
        if not isinstance(self.config, np.ndarray):
            sys.exit("WARNING CONFIG NOT NUMPY ARRAY, PROGRAM TERMINATED")
        if self.config.shape != (self.Natoms,3):
            sys.exit("WARNING POSITIONS NOT OF DIMENSION (Natoms,3), PROGRAM TERMINATED")
