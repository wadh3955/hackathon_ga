import numpy as np
import Cluster_type
import random
import math
#from sobol import *

def Mate(P1,P2):
    """Creates a child from two parents P1 and P2, using a cutoff in P1 (Does not preserve ratio of A to B)"""

    # Pick a random atom from parent 1 to be the cutoff point (CP) and find its position pos_cp
    CP = random.randint(0, P1.Natoms-1)
    pos_cp = np.array([P1.config[CP, 0], P1.config[CP, 1]])
    # Randomly generate the number of atoms to copy from parent 1 (S)
    S = random.randint(1, P1.Natoms-1)

    # Create an array of the distances of atoms in P1 from the CP
    distance_P1 = []
    for i in range(P1.Natoms):
        dist = math.sqrt((P1.config[i,0]-pos_cp[0])**2.0 + (P1.config[i,1]-pos_cp[1])**2.0)
        distance_P1.append(dist)

    # Copy the Parent.config list in order to sort it by distance from CP
    SP1 = P1.config.tolist()
    # Order the atoms in parent 1 by their distance from CP
    SP1=[x for (y, x) in sorted(zip(distance_P1, SP1))]

    # Create the child config array and copy closest S atoms to the CP in parent 1 to the child
    D1= []
    for i in range(S):
        D1.append(SP1[i])

    #print "D1", D1

    # Create an array of the distances of atoms in P2 from the CP
    distance_P2 = []
    for i in range(P1.Natoms):
        dist = math.sqrt((P2.config[i, 0] - pos_cp[0]) ** 2.0 + (P2.config[i, 1] - pos_cp[1]) ** 2.0)
        distance_P2.append(dist)
    # Copy the Parent.config list for parent 2 in order to sort it by distance from CP
    SP2 = P2.config.tolist()
    # Order the atoms in parent 2 by their distance from CP
    SP2 = [x for (y, x) in sorted(zip(distance_P2, SP2))]

    #print "SP2", SP2

    k=0
    for_del = []
    for i in SP2:
        for j in D1:
            dist = math.sqrt((i[0]-j[0]) ** 2.0 + (i[1] - j[1]) ** 2.0)
            if dist < 0.5:
                for_del.append(k)
                #print "I'm being deleted from SP2", SP2[k]
                break
        k += 1
    SP2 = [ item for i,item in enumerate(SP2) if i not in for_del ]

    # Check if there are enough atoms left in Parent 2 to finish the child, if not randomly generate remain atoms
    if len(SP2) >= P1.Natoms - S:
        for i in range(P1.Natoms - S):
            D1.append(SP2[i])
    else:
        for i in SP2:
            D1.append(i)
        #seed = random.randint(100, 1000)  # produce random seed for sobol sequence
        for i in range(P1.Natoms - S - len(SP2)):
            #cooridnates, seed = i4_sobol(2,seed)  # produce sobol sequence, these are pseudo random number that are more uniformly spaced
            if random.random() > 0.5:
                D1.append([P1.box_len * random.random(), P1.box_len * random.random(), 1])
            else:
                D1.append([P1.box_len * random.random(), P1.box_len * random.random(), 0])

    # Instanciate a new cluster object and change its positions to those of the child
    C1 = Cluster_type.Cluster(P1.Natoms,P1.rad1)
    C1.config = np.array(D1)
    C1.energy_update()

    # print "D1", D1
    # print "SP1", SP1
    # print "SP2", SP2
    # print "CP", pos_cp

    return C1

if __name__ == "__main__" :
    P1 = Cluster_type.Cluster(5,1.0)
    P2 = Cluster_type.Cluster(5,1.0)
    C1 = Mate(P1,P2)
    print P1.config
    print P2.config
    print C1.config
    #print C2.config

