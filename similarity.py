# Module to test similarity of two selected clusters
import numpy as np

def COM(cluster):
    # First find the centre of mass of the cluster (assume mass = 1)
    N = cluster.Natoms
    xsum = 0
    ysum = 0
    i = 0
    for atom in cluster.config:
        xsum += cluster.config[i,0]
        ysum += cluster.config[i,1]
        i += 1
    # x and y coordinates of the centre of mass are sum of positions over N
    xcom = xsum/N
    ycom = ysum/N
    com = [xcom, ycom]
    # Output array of COM coordinates
    com = np.array(com)
    return com

def distance_from_COM(cluster):
    # Calculates the distances from the COM for each atom in cluster
    com = COM(cluster)
    i = 0
    ord = []
    for atom in cluster.config:
        vector = (cluster.config[i,0] - com[0])**2 + (cluster.config[i,1] - com[1])**2
        ord.append(vector)
    # Return distances as a numpy array
    ord = np.array(ord)
    # Put the distances in order
    ord = np.sort(ord)
    return ord

def similarity_test(parent, child):
    # Make sure that the number of atoms matches in both clusters
    N = parent.Natoms
    if N == child.Natoms:
        # Import the ord vectors for each cluster
        ordparent = distance_from_COM(parent)
        ordchild = distance_from_COM(child)
        # print ordparent, ordchild
        # Calculate d for the two clusters
        d = 0
        for atom in range(0,N):
            sum = ordparent[atom] - ordchild[atom]
            sum = abs(sum)
            sum = sum**3
            d += sum
        # Return value of d^3
        return d
    else:
        print "Number of atoms in clusters does not match"

if __name__ == "__main__" :
    print similarity_test(parent, child)







