import numpy as np
import sys
import random
import Cluster_energy
import similarity
import mutate
from sobol import *

class Cluster:
    def __init__(self, Natoms,radius1):
        self.rad1 = radius1
        self.Natoms = Natoms
        self.box_len = np.sqrt(self.Natoms*np.pi*5*(self.rad1*1.123/2)**2)
        while True:
            config = []
            #seed = random.randint(100,100000)    # produce random seed for sobol sequence
            for i in range(Natoms):
                #cooridnates, seed = i4_sobol(2, seed)  # produce sobol sequence, these are pseudo random number that are more uniformly spaced
                if random.random() > 0.5:
                    config.append([self.box_len*random.random(),self.box_len*random.random(),1])
                else:
                    config.append([self.box_len*random.random(),self.box_len*random.random(), 0])
            self.config=np.array(config)
            self.energy =  Cluster_energy.energy(self)
            if self.energy < -0.5 :
                False
                break
            else:
                True
        self.displacement = similarity.distance_from_COM(self)
    def energy_update(self):
        self.energy = Cluster_energy.energy(self)
    def displacement_update(self):
        self.displacement = similarity.distance_from_COM(self)
    def cluster_mutation(self):
        self.config = mutate.mutation(self.config, self.rad1, self.Natoms)
    def cluster_check(self):
        if not isinstance(self.config, np.ndarray):
            sys.exit("WARNING CONFIG NOT NUMPY ARRAY, PROGRAM TERMINATED")
        if self.config.shape != (self.Natoms,3):
            sys.exit("WARNING POSITIONS NOT OF DIMENSION (Natoms,3), PROGRAM TERMINATED")
