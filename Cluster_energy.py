import numpy as np
import Cluster_type



def energy(cluster):

    i=0
    vsum = 0
    for atomi in cluster.config:
        j=0
        vj =0
        # sigma BB chosen from {1.05, 1.1, 1.15, 1.2, 1.25, 1.3}
        sigmaAA = 1
        sigmaBB = cluster.rad1
        sigmaAB = (sigmaAA+sigmaBB)/2
        for atomj in cluster.config:
            if i == j:
                j += 1
                v = 0
                vj = vj + v

            else:
                # assign sigma value based on particle being numbered 0 or 1
                if cluster.config[i, 2] < 0.5 and cluster.config[j, 2] < 0.5:
                    sigma = sigmaAA
                elif cluster.config[i, 2] > 0.5 and cluster.config[j, 2] > 0.5:
                    sigma = sigmaBB
                else:
                    sigma = sigmaAB

                # calculate distance between particles
                r = ((cluster.config[i, 0]-cluster.config[j, 0])**2+(cluster.config[i, 1]-cluster.config[j, 1])**2)**0.5
                # calculate energy for interaction
                v = 4*((sigma/r)**12-(sigma/r)**6)
                # sum energies for all interactions for particle i
                vj = vj + v
                j += 1

        # total energy of cluster
        vsum = vsum + vj
        i += 1

    vsum = vsum/2
    return vsum


if __name__ == "__main__":
    cluster1 = Cluster_type.Cluster(2, 1)
    cluster1.config = np.array([[0, 0, 0], [1.1224, 0, 1]])

    energy = energy(cluster=cluster1)
    print energy
